<?php

namespace App\Http\Controllers\API;

use Config;
use App\News;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){
        $news = News::orderBy('title', 'asc')->orderBy('date', 'asc');

        if($request->date)
            $news = $news->where('date', '=', $date);

        if($request->title)
            $news = $news->where('title', '=', $title);

        $data['news'] = $news->paginate(Config::get('settings.pageLimit'));

        return response()->json(['success' => true, 'data' => $data], $this->successStatus);
    }

    /**
     * Store a newly ::orderBy(array('title', 'date'))created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $validator = Validator::make($request->all(),[
            'title' => 'required',
            'date' => 'required|date|date_format:Y-m-d',
            'short_desc' => 'required',
            'text' => 'required',
            ]);

        if($validator->fails()){
            return response()->json($validator->errors(), $this->badRequest);
        }

        if($data['news'] = News::create($request->all()))
            return response()->json(['success' => true, 'data' => $data], $this->createdStatus);

        return response()->json(['success' => false, 'message' => 'something went wrong'], $this->wrongStatus);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(News $news){
        return response()->json(['success' => true, 'data' => $news], $this->successStatus);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, News $news){
        if($news->update($request->all())){
            $data['news'] = $news;
            return response()->json(['success' => true, 'data' => $data], $this->acceptedStatus);
        }
        return response()->json(['success' => false, 'message' => 'something went wrong'], $this->wrongStatus);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(News $news){
        if($news->delete())
            return response()->json(['success' => true], $this->successStatus);
        return response()->json(['success' => false, 'message' => 'something went wrong'], $this->wrongStatus);
    }
}
