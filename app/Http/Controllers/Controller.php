<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $successStatus = 200;

    public $createdStatus = 201;

    public $acceptedStatus = 202;

    public $emptyStatus = 204;

    public $badStatus = 400;

    public $unauthorizedStatus = 401;

    public $badRequest = 409;

    public $wrongStatus = 500;
}
